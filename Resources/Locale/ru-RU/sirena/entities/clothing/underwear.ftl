ent-ClothingUnderwearSocksHigh = высокие носки
    .desc = Высокие носки Nanotrasen для сотрудников станции. Покрашены в белый цвет.
    .suffix = { "" }

ent-ClothingUnderwearSocksNormal = носки
    .desc = Стандартные носки Nanotrasen для сотрудников станции. Покрашены в белый цвет.
    .suffix = { "" }

ent-ClothingUnderwearSocksShort = короткие носки
    .desc = Короткие носки Nanotrasen для сотрудников станции. Покрашены в белый цвет.
    .suffix = { "" }

ent-ClothingUnderwearSocksThigh = чулки
    .desc = Стандартные чулки Nanotrasen для сотрудников станции. Покрашены в белый цвет.
    .suffix = { "" }

ent-ClothingUnderwearBottomPantiesWhite = трусики
    .desc = Стандартное нижнее бельё Nanotrasen для сотрудниц станции. Покрашены в белый цвет.
    .suffix = { "" }

ent-ClothingUnderwearBottomBoxersWhite = боксеры
    .desc = Стандартное нижнее бельё Nanotrasen для сотрудников станции. Покрашены в белый цвет.
    .suffix = { "" }

ent-ClothingUnderwearTopBraWhite = бра
    .desc = Стандартный лифчик Nanotrasen для сотрудниц станции. Покрашен в белый цвет.
    .suffix = { "" }

ent-ClothingUnderwearTopBraSports = спортивное бра
    .desc = Лифчик для занятий спортом. Покрашен в белый цвет.
    .suffix = { "" }

ent-ClothingUnderwearTopBraSportsAlternative = альтернативное спортивное бра
    .desc = Лифчик для занятий спортом. Альтернативная версия. Покрашен в белый цвет.
    .suffix = { "" }
	
ent-ClothingUnderwearBottomBoxersCap = боксеры капитана
    .desc = Стандартное нижнее бельё капитана станции.
    .suffix = { "" }

ent-ClothingUnderwearBottomBoxersCE = боксеры СИ
    .desc = Стандартное нижнее бельё старшено инженера станции.
    .suffix = { "" }

ent-ClothingUnderwearBottomBoxersCMO = боксеры ГВ
    .desc = Стандартное нижнее бельё главного врача станции. Почему-то они пахнут хлоралгидратом...
    .suffix = { "" }

ent-ClothingUnderwearBottomBoxersHOP = боксеры ГП
    .desc = Стандартное нижнее бельё главы персонала станции.
    .suffix = { "" }

ent-ClothingUnderwearBottomBoxersHOS = боксеры ГСБ
    .desc = Стандартное нижнее бельё главы службы безопасности станции.
    .suffix = { "" }

ent-ClothingUnderwearBottomBoxersQM = боксеры КМ
    .desc = Стандартное нижнее бельё квартирмейстера станции.
    .suffix = { "" }

ent-ClothingUnderwearBottomBoxersRD = боксеры НР
    .desc = Стандартное нижнее бельё научного руководителя станции.
    .suffix = { "" }

ent-ClothingUnderwearBottomPantiesCap = трусики капитана
    .desc = Стандартное нижнее бельё капитана станции.
    .suffix = { "" }

ent-ClothingUnderwearBottomPantiesCE = трусики СИ
    .desc = Стандартное нижнее бельё старшено инженера станции.
    .suffix = { "" }

ent-ClothingUnderwearBottomPantiesCMO = трусики ГВ
    .desc = Стандартное нижнее бельё главного врача станции. Почему-то они пахнут хлоралгидратом...
    .suffix = { "" }

ent-ClothingUnderwearBottomPantiesHOP = трусики ГП
    .desc = Стандартное нижнее бельё главы персонала станции.
    .suffix = { "" }

ent-ClothingUnderwearBottomPantiesHOS = трусики ГСБ
    .desc = Стандартное нижнее бельё главы службы безопасности станции.
    .suffix = { "" }

ent-ClothingUnderwearBottomPantiesQM = трусики КМ
    .desc = Стандартное нижнее бельё квартирмейстера станции.
    .suffix = { "" }

ent-ClothingUnderwearBottomPantiesRD = трусики НР
    .desc = Стандартное нижнее бельё научного руководителя станции.
    .suffix = { "" }
	
ent-ClothingUnderwearTopBraCap = бра капитана
    .desc = Стандартное нижнее бельё капитана станции.
    .suffix = { "" }

ent-ClothingUnderwearTopBraCE = бра СИ
    .desc = Стандартное нижнее бельё старшено инженера станции.
    .suffix = { "" }

ent-ClothingUnderwearTopBraCMO = бра ГВ
    .desc = Стандартное нижнее бельё главного врача станции. Почему-то оно пахнет хлоралгидратом...
    .suffix = { "" }

ent-ClothingUnderwearTopBraHOP = бра ГП
    .desc = Стандартное нижнее бельё главы персонала станции.
    .suffix = { "" }

ent-ClothingUnderwearTopBraHOS = бра ГСБ
    .desc = Стандартное нижнее бельё главы службы безопасности станции.
    .suffix = { "" }

ent-ClothingUnderwearTopBraQM = бра КМ
    .desc = Стандартное нижнее бельё квартирмейстера станции.
    .suffix = { "" }

ent-ClothingUnderwearTopBraRD = бра РД
    .desc = Стандартное нижнее бельё научного руководителя станции.
    .suffix = { "" }
